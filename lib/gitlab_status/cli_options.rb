require 'optparse'

module GitlabStatus
  Options = Struct.new(:url, :interval, :period, :help)

  # Processes command line arguments
  class CliOptions
    class << self
      def parse(options) # rubocop:disable MethodLength
        args = Options.new('https://gitlab.com', 10, 60, false)

        opt_parser = OptionParser.new do |opts|
          opts.banner = 'Usage: gitlab_status [options]'

          opts.on(
            '-u <url>',
            '--url=<url>',
            'The url to check status on. The default is https://gitlab.com.'
          ) do |u|
            args.url = u
          end

          opts.on(
            '-i <seconds>',
            '--interval=<seconds>',
            'Poll the site url at the provided interval. '\
            'The default is 10 seconds.'
          ) do |i|
            args.interval = i.to_i
          end

          opts.on(
            '-p <seconds>',
            '--period=<seconds>',
            'Poll the site url over the specified period. '\
            'The default is 60 seconds.'
          ) do |p|
            args.period = p.to_i
          end

          opts.on('-h', '--help', 'Displays command line help. ') do
            puts opts
            args.help = true
          end
        end

        opt_parser.parse!(options)
        args
      end
    end
  end
end
