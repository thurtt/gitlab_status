require 'benchmark'
require 'gitlab_status/errors'
require 'net/http'
require 'uri'

module GitlabStatus
  # Queries the status and response time for http protocol urls
  class HttpStatus
    def response_time(url)
      resp = nil

      # Benchmark the total request time
      # This covers both time up and time back,
      # but that is a valid real-world response time.
      real_time = Benchmark.realtime do
        resp = perform_request(url)
      end

      # We follow redirects, so anything other than 200
      # is an error state
      raise RequestError(resp) unless resp.code == '200'

      real_time
    end

    private

    def perform_request(url)
      uri = URI.parse(url)
      raise InvalidURLError unless uri.respond_to?(:request_uri)

      http = Net::HTTP.new(uri.host, uri.port)

      # use ssl if given an https protocol
      http.use_ssl = (uri.scheme =~ /https/i)

      # perform a GET at the address
      req = Net::HTTP::Get.new(uri.request_uri)
      resp = http.request(req)

      # follow any redirects
      if %w[301 302].include?(resp.code)
        resp = perform_request(resp['location'])
      end

      resp
    end
  end
end
