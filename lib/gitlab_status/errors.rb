module GitlabStatus
  # All gitlab_status errors derive from this base error
  class GitlabStatusError < StandardError
  end

  class InvalidURLError < GitlabStatusError
  end

  class RequestError < GitlabStatusError
  end
end
