require 'gitlab_status/version'
require 'gitlab_status/http_status'
require 'gitlab_status/errors'

module GitlabStatus
  # Polls website status
  class SiteStatus
    class << self
      def average_response_time(url:, sample_interval: 10, sample_period: 60)
        resp_times = []
        http_status = HttpStatus.new

        poll_for(interval: sample_interval, period: sample_period) do
          resp_times << http_status.response_time(url)
        end

        resp_times.reduce(:+) / resp_times.size
      end

      private

      def poll_for(interval:, period:)
        # Use the monotonic clock for a more acurate timing
        start_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
        loop do
          yield
          stop_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)

          # Don't exceed the maximum period
          break if ((stop_time - start_time) + interval) >= period
          sleep(interval)
        end
      end
    end
  end
end
