require 'bundler/setup'
require 'gitlab_status'
require 'gitlab_status/cli_options'

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = '.rspec_status'

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  # Use only the expect syntax
  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end
