RSpec.describe GitlabStatus::CliOptions do
  describe '.parse' do
    shared_examples 'a command line argument parser' do
      it 'uses the correct url' do
        expect(args.url).to eq parsed_values[:url]
      end

      it 'uses the correct interval' do
        expect(args.interval).to eq parsed_values[:interval]
      end

      it 'uses the correct period' do
        expect(args.period).to eq parsed_values[:period]
      end
    end

    context 'when no options are provided' do
      let(:args) { GitlabStatus::CliOptions.parse([]) }
      let(:parsed_values) do
        { url: 'https://gitlab.com', interval: 10, period: 60 }
      end

      it_should_behave_like 'a command line argument parser'
    end

    context 'when short options are provided' do
      let(:args) do
        GitlabStatus::CliOptions.parse(
          ['-u', 'https://about.gitlab.com', '-i', '5', '-p', '300']
        )
      end
      let(:parsed_values) do
        { url: 'https://about.gitlab.com', interval: 5, period: 300 }
      end

      it_should_behave_like 'a command line argument parser'
    end

    context 'when long options are provided' do
      let(:args) do
        GitlabStatus::CliOptions.parse(
          ['--url=https://about.gitlab.com', '--interval=5', '--period=300']
        )
      end
      let(:parsed_values) do
        { url: 'https://about.gitlab.com', interval: 5, period: 300 }
      end

      it_should_behave_like 'a command line argument parser'
    end
  end
end
