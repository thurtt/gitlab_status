RSpec.describe GitlabStatus::SiteStatus do
  describe '.average_response_time' do
    shared_examples 'a website status check' do |interval, period|
      it 'returns an average response time for'\
        "interval: #{interval}s, period: #{period}s" do
        expect(
          GitlabStatus::SiteStatus.average_response_time(
            url: url,
            sample_interval: interval,
            sample_period: period
          )
        ).to be > 0
      end
    end

    context 'when polling gitlab.com' do
      let(:url) { 'https://gitlab.com' }

      it_should_behave_like 'a website status check', 1, 15
      it_should_behave_like 'a website status check', 5, 30
      it_should_behave_like 'a website status check', 10, 60

      it 'should not take much longer than the period' do
        time = Benchmark.realtime do
          GitlabStatus::SiteStatus.average_response_time(
            url: url,
            sample_interval: 2,
            sample_period: 15
          )
        end
        expect(time).to be <= 16
      end
    end

    context 'when polling about.gitlab.com' do
      let(:url) { 'https://about.gitlab.com' }

      it_should_behave_like 'a website status check', 1, 15
      it_should_behave_like 'a website status check', 5, 30
      it_should_behave_like 'a website status check', 10, 60
    end
  end
end
