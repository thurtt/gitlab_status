RSpec.describe GitlabStatus::HttpStatus do
  describe '#response_time' do
    let(:http_status) { GitlabStatus::HttpStatus.new }

    context 'when using a valid URL' do
      let(:gitlab_url) { 'https://gitlab.com/' }
      let(:about_url) { 'https://about.gitlab.com' }

      it 'returns the response time to gitlab.com' do
        expect(http_status.response_time(gitlab_url)).to be > 0.0
      end

      it 'returns the response time to about.gitlab.com' do
        expect(http_status.response_time(about_url)).to be > 0.0
      end
    end

    context 'when given an invalid URL' do
      let(:invalid_url) { 'not//even:valid' }

      it 'raises an invalid URL error' do
        expect { http_status.response_time(invalid_url) }.to(
          raise_error(GitlabStatus::InvalidURLError)
        )
      end
    end
  end
end
