# GitlabStatus

GitlabStatus is a ruby gem that polls the status of the gitlab web application and reports the average response time.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'gitlab_status', git: 'https://gitlab.com/thurtt/gitlab_status.git'
```

And then execute:
```
$ bundle
```

## Usage

### To run the console application

The default configuration polls https://gitlab.com every 10 seconds for a period of 60 seconds.
```
$ bin/gitlab_status
```

You can also specify the url, poll interval, and poll period.
```
$ bin/gitlab_status --url=https://about.gitlab.com --interval=1 --period=30
```

### To include into your project

```ruby
require 'gitlab_status'
resp_time = GitlabStatus::SiteStatus.average_response_time(
  url: 'https://gitlab.com',
  sample_interval: 2,
  sample_period: 60
)
```

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
