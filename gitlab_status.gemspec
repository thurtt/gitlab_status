
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'gitlab_status/version'

Gem::Specification.new do |spec|
  spec.name          = 'gitlab_status'
  spec.version       = GitlabStatus::VERSION
  spec.authors       = ['Thomas Hurtt']
  spec.email         = ['thurtt@gmail.com']

  spec.summary       = 'A Ruby Gem to query the status of gitlab'
  spec.description   = 'This Gem will query the status of gitlab and report \
                        the average response time.'
  spec.homepage      = 'https://gitlab.com/thurtt/gitlab_status'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'bin'
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.required_ruby_version = '>= 2.3.4'

  spec.add_development_dependency 'bundler', '~> 1.16'
  spec.add_development_dependency 'pry', '~> 0.11.3'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'rubocop', '~> 0.52.1'
end
